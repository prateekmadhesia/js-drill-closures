module.exports = function counterFactory() {
  
  let outerVariable=0; //closer variable

  function increaseOuterVariable(n=1){
    outerVariable+=n;
    console.log(`Incremented value by ${n}`);
    return outerVariable;
  }

  function decreaseOuterVariable(n=1){
    outerVariable-=n;
    console.log(`Decrement value by ${n}`);
    return outerVariable;
  }

  return {increment : increaseOuterVariable, decrement : decreaseOuterVariable}
}