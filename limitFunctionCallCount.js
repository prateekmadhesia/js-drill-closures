module.exports = function limitFunctionCallCounter(callback, limit){

  if(typeof callback == undefined || typeof callback != 'function'){
    return;
  }
  //it will count, how many times inner function was called.
  let innerFunctionCounter=0; 

  function callbackFunctionInvoker(){
    
    innerFunctionCounter+=1;

    if(innerFunctionCounter <=  limit){
      console.log(innerFunctionCounter);
      return callback(); //invoke callback
    }
    else{
      return null;
    }
  }

  return callbackFunctionInvoker;
}