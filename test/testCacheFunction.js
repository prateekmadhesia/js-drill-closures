const cacheFunction = require('../cacheFunction.js');

function sum2(a,b){
  return a+b;
}
function sum3(a,b,c){
  return a+b+c;
}

function sumAllValues(arr){
  return arr.reduce((acc, curr) =>{
    return acc + curr;
  }, 0);
}

const sum2OuterFunction = cacheFunction(sum2);
const sum2Output1 = sum2OuterFunction(2,3);
const sum2Output2 = sum2OuterFunction(20,30);
const sum2Output3 = sum2OuterFunction(2,3);
console.log(sum2Output1, sum2Output2, sum2Output3);

const sum3OuterFunction = cacheFunction(sum3);
const sum3Output1 = sum3OuterFunction(2,3,5);
const sum3Output2 = sum3OuterFunction(20,30,100);
const sum3Output3 = sum3OuterFunction(2,3,5);
console.log(sum3Output1, sum3Output2, sum3Output3);

const sumAllValuesOuterFunction = cacheFunction(sumAllValues);
const sumAllValuesOutput1 = sumAllValuesOuterFunction([1,2,3]);
const sumAllValuesOutput2 = sumAllValuesOuterFunction([20,30,40]);
const sumAllValuesOutput3 = sumAllValuesOuterFunction([1,2,3]);
console.log(sumAllValuesOutput1, sumAllValuesOutput2, sumAllValuesOutput3);

const doubleOuterFunction = cacheFunction((arr) =>{
  if(arr==undefined)
    return;
  return arr.map((a) => {
    return 2*a;
  });
});
const outputDoubleValues1 = doubleOuterFunction([1,2,3]);
const outputDoubleValues2 = doubleOuterFunction([10,20,30]);
const outputDoubleValues3 = doubleOuterFunction([1,2,3]);
console.log(outputDoubleValues1, outputDoubleValues2, outputDoubleValues3);
