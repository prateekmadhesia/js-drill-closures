const count = require('../counterFactory');

// calling outer function first.
const counter = count(); 

// Now calling inner functions.
let increase1 = counter.increment(5);
console.log(increase1);

let increase2 = counter.increment();
console.log(increase2);

let increase3 = counter.increment();
console.log(increase3);

let decrease1 = counter.decrement();
console.log(decrease1);

let decrease2 = counter.decrement(4);
console.log(decrease2);

let decrease3 = counter.decrement();
console.log(decrease3);