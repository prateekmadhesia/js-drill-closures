const count = require('../limitFunctionCallCount.js');

function callback(){
  console.log("Callback invoked");
}

const outerFunction1 = count(callback, 5);
const outerFunction2 = count(callback, 3);

for(let i=0; i<5; i++){
  if(outerFunction1 != undefined){
    outerFunction1();
  }
  
  if(outerFunction2 != undefined){
    outerFunction2();
  }
}
