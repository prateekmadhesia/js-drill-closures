module.exports = function cacheFunction(callback) {
  if(callback == undefined)
    return;
  const cache = {};

  function callbackFunctionInvoker(...args) { 
    if(args==undefined)
      return;
    let allArguments=args.toString();

    if(cache[allArguments]==undefined){
      cache[allArguments]=callback(...args);
      return cache[allArguments];
    }
    else
      return cache[allArguments];
  }

  return callbackFunctionInvoker;
}